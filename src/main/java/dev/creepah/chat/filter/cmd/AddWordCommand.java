package dev.creepah.chat.filter.cmd;

import dev.creepah.chat.ChatCore;
import org.bukkit.command.CommandSender;
import tech.rayline.core.command.ArgumentRequirementException;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("chatfilter.add")
public final class AddWordCommand extends RDCommand {

    public AddWordCommand() {
        super("add");
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        if (args.length == 0) throw new ArgumentRequirementException("Invalid arguments! Usage: /chatfilter add <word>");

        ChatCore i = ChatCore.get();
        i.getChatFilter().addWord(args[0]);

        sender.sendMessage(i.formatAt("word-added").withModifier("word", args[0].toLowerCase()).get());
    }
}
