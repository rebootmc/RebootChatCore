package dev.creepah.chat.filter.cmd;

import dev.creepah.chat.ChatCore;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("chatfilter.command")
@CommandMeta(description = "Toggle your chat filter.")
public final class FilterCommand extends RDCommand {

    public FilterCommand() {
        super("chatfilter", new AddWordCommand(), new RemoveWordCommand());
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        ChatCore i = ChatCore.get();
        boolean status = i.getChatFilter().toggle(player);

        player.sendMessage(i.formatAt("filter-toggled").withModifier("status", status ? "&cdisabled" : "&aenabled").get());
    }
}
