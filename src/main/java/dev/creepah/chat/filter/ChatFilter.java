package dev.creepah.chat.filter;

import dev.creepah.chat.ChatCore;
import lombok.Getter;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;

import java.util.List;

public final class ChatFilter {

    @Getter private List<String> bannedWords;
    @Getter private List<String> disabledList;
    private final String[] punctuation = new String[] {
            ".", "'", ",", ";", "/", "*", "-", "#", "@", "$", "!", "<",
            ">", "%", "^", "&", "|", "(", ")", "+", "=", "~", "`", "s", "_"
    };

    public ChatFilter() {
        ChatCore i = ChatCore.get();

        bannedWords = i.getConfig().getStringList("banned-words");
        disabledList = i.getStorageYML().getStringList("filter-disabled");
    }

    // Save the banned words and filter disabled list to relevant files upon disable.
    public void save() {
        ChatCore i = ChatCore.get();

        i.getConfig().set("banned-words", bannedWords);
        i.getStorageYML().set("filter-disabled", disabledList);
        i.saveConfig();
    }

    // Return a filtered version of the provided string.
    public String filterMessage(String message) {
        for (String word : bannedWords) {
            message = message.replaceAll(formRegex(word), getAsteriskString(word.length()));
        }

        return message;
    }

    // Forms a regex matching string for the provided word.
    public String formRegex(String word) {
        char[] chars = word.toCharArray();
        StringBuilder builder = new StringBuilder();

        builder.append("\\b");
        for (int i = 0; i < chars.length; i++) {
            builder.append("[").append(String.valueOf(chars[i]).toLowerCase()).append(String.valueOf(chars[i]).toUpperCase()).append(']');
            if (i != chars.length - 1) {
                builder.append("+").append("[");
                for (String p : punctuation) builder.append("\\").append(p);
                builder.append("\\d]*");
            }
        }
        builder.append("\\b");

        return builder.toString().trim();
    }

    // Returns an appropriately sized string of asterisks based upon the provided word length.
    public String getAsteriskString(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) sb.append("*");

        return sb.toString().trim();
    }

    // Adds a word to the banned words list.
    public void addWord(String word) throws CommandException {
        word = word.toLowerCase();
        if (bannedWords.contains(word)) throw new CommandException("That word is already banned");

        bannedWords.add(word);
    }

    // Removes a word from the banned words list.
    public void removeWord(String word) throws CommandException {
        word = word.toLowerCase();
        if (!bannedWords.contains(word)) throw new CommandException("That word is not banned");

        bannedWords.remove(word);
    }

    public boolean isFilteringChat(Player player) {
        return !getDisabledList().contains(player.getUniqueId().toString());
    }

    // Returns true if added to list, false if removed.
    public boolean toggle(Player player) {
        String uuid = player.getUniqueId().toString();

        if (disabledList.contains(uuid)) {
            disabledList.remove(uuid);
            return false;
        } else {
            disabledList.add(uuid);
            return true;
        }
    }
}
