package dev.creepah.chat.factionschat;

import org.bukkit.entity.Player;

import java.util.*;

public final class FactionsChat {

    private Map<UUID, ChatType> chatTypes = new HashMap<>();
    private List<UUID> spies = new ArrayList<>();

    // Get the provided player's chat type.
    public ChatType getChatType(Player player) {
        return chatTypes.containsKey(player.getUniqueId()) ? chatTypes.get(player.getUniqueId()) : ChatType.PUBLIC;
    }

    // Set the provided player's chat type.
    public void setChatType(Player player, ChatType type) {
        chatTypes.put(player.getUniqueId(), type);
    }

    // Toggles chat spy for the specified player.
    public boolean toggleChatSpy(Player player) {
        UUID uuid = player.getUniqueId();

        if (spies.contains(uuid)) {
            spies.remove(uuid);
            return false;
        } else {
            spies.add(uuid);
            return true;
        }
    }

    // Returns true if the specified player is spying on factions chat.
    public boolean isSpying(Player player) {
        return player.hasPermission("factions.chat.spy") && spies.contains(player.getUniqueId());
    }
}
