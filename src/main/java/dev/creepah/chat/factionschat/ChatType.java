package dev.creepah.chat.factionschat;

import com.massivecraft.factions.Rel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ChatType {
    PUBLIC("Public", new ArrayList<>(), "fc.public", "p"),
    ALLY("Ally", Arrays.asList(Rel.ALLY, Rel.MEMBER, Rel.LEADER, Rel.OFFICER, Rel.RECRUIT), "fc.ally", "a"),
    FACTION("Faction", Arrays.asList(Rel.MEMBER, Rel.LEADER, Rel.OFFICER, Rel.RECRUIT), "fc.faction", "f"),
    TRUCE("Truce", Arrays.asList(Rel.MEMBER, Rel.LEADER, Rel.OFFICER, Rel.RECRUIT, Rel.TRUCE), "fc.truce", "t"),
    ALLYTRUCE("Ally + Truce", Arrays.asList(Rel.MEMBER, Rel.LEADER, Rel.OFFICER, Rel.RECRUIT, Rel.TRUCE, Rel.ALLY), "fc.allytruce", "at");

    private String string; // A pretty string to use in the format.
    private List<Rel> relationships; // An array of relationships which this chat will send messages to.
    private String format; // Reference to chat format.
    private String shortForm;

    ChatType(String string, List<Rel> relationships, String format, String shortForm) {
        this.string = string;
        this.relationships = relationships;
        this.format = format;
        this.shortForm = shortForm;
    }

    public String getString() {
        return string;
    }

    public List<Rel> getRelationships() {
        return relationships;
    }

    public String getFormat() {
        return format;
    }

    public String getShortForm() {
        return shortForm;
    }
}
