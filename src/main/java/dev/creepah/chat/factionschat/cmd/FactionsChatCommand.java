package dev.creepah.chat.factionschat.cmd;

import com.massivecraft.factions.entity.MPlayer;
import dev.creepah.chat.ChatCore;
import dev.creepah.chat.factionschat.ChatType;
import dev.creepah.chat.factionschat.FactionsChat;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public final class FactionsChatCommand {

    // Actually a normal class with a listener on pre-process.

    public FactionsChatCommand() {
        ChatCore i = ChatCore.get();
        FactionsChat factionsChat = i.getFactionsChat();

        i.observeEvent(PlayerCommandPreprocessEvent.class)
                .filter(event -> {
                    String[] msg = event.getMessage().toLowerCase().split(" ");
                    return msg.length >= 2 && msg[0].equals("/f") && (msg[1].equals("c") || msg[1].equals("chat"));
                })
                .subscribe(event -> {
                    Player player = event.getPlayer();
                    String[] args = event.getMessage().split(" "); // args[2] onwards are actual arguments past ./f chat!
                    ChatType type = null;

                    if (!player.hasPermission("factions.chat")) {
                        player.sendMessage(i.formatAt("no-chat-perm").get());
                        event.setCancelled(true);
                        return;
                    }

                    if (MPlayer.get(player).getFaction().isNone()) {
                        player.sendMessage(i.formatAt("need-fac").get());
                        event.setCancelled(true);
                        return;
                    }

                    // If they specify a chat mode..
                    if (args.length > 2) {
                        for (ChatType t : ChatType.values()) {
                            if (args[2].equalsIgnoreCase(t.getString()) || args[2].equalsIgnoreCase(t.getShortForm())) type = t;
                        }

                        if (type == null) {
                            player.sendMessage(i.formatAt("invalid-type").get());
                            return;
                        }

                        factionsChat.setChatType(player, type);
                    } else {
                        factionsChat.setChatType(player, nextType(factionsChat.getChatType(player)));
                    }

                    player.sendMessage(i.formatAt("chat-mode-set").withModifier("type", factionsChat.getChatType(player).getString()).get());
                    event.setCancelled(true);
                });
    }

    // Get the next chat type.
    public ChatType nextType(ChatType type) {
        // If it's on the last value, reset back to first.
        if (type.ordinal() + 1 == ChatType.values().length) return ChatType.PUBLIC;

        for (ChatType t : ChatType.values()) {
            if (t.ordinal() == type.ordinal() + 1) return t;
        }

        return ChatType.PUBLIC;
    }
}
