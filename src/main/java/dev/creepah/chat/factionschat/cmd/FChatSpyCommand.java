package dev.creepah.chat.factionschat.cmd;

import dev.creepah.chat.ChatCore;
import org.bukkit.entity.Player;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("factions.chat.spy")
@CommandMeta(description = "Spy on faction chat", aliases = {"fchatspy", "fcs"})
public final class FChatSpyCommand extends RDCommand {

    public FChatSpyCommand() {
        super("fcspy");
    }

    @Override
    protected void handleCommand(Player player, String[] args) throws CommandException {
        ChatCore i = ChatCore.get();

        boolean status = i.getFactionsChat().toggleChatSpy(player);
        player.sendMessage(i.formatAt("fchat-spy-toggled").withModifier("status", status ? "&aenabled" : "&cdisabled").get());
    }
}
