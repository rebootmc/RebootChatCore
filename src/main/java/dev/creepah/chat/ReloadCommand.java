package dev.creepah.chat;

import org.bukkit.command.CommandSender;
import tech.rayline.core.command.CommandException;
import tech.rayline.core.command.CommandMeta;
import tech.rayline.core.command.CommandPermission;
import tech.rayline.core.command.RDCommand;

@CommandPermission("chatcore.reload")
@CommandMeta(description = "Reload chat core's config file")
public final class ReloadCommand extends RDCommand {

    public ReloadCommand() {
        super("chatcorereload");
    }

    @Override
    protected void handleCommandUnspecific(CommandSender sender, String[] args) throws CommandException {
        ChatCore.get().reloadConfig();
        sender.sendMessage(ChatCore.get().formatAt("reloaded").get());
    }
}
