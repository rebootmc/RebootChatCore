package dev.creepah.chat;

import dev.creepah.chat.factionschat.FactionsChat;
import dev.creepah.chat.factionschat.cmd.FChatSpyCommand;
import dev.creepah.chat.factionschat.cmd.FactionsChatCommand;
import dev.creepah.chat.filter.ChatFilter;
import dev.creepah.chat.filter.cmd.FilterCommand;
import dev.creepah.chat.item.ItemChat;
import lombok.Getter;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import tech.rayline.core.plugin.RedemptivePlugin;
import tech.rayline.core.plugin.UsesFormats;

import java.io.File;

@UsesFormats
public final class ChatCore extends RedemptivePlugin {

    private static ChatCore instance;

    public static ChatCore get() {
        return instance;
    }

    @Getter private FileConfiguration storageYML;
    @Getter private ChatFilter chatFilter;
    @Getter private ItemChat itemChat;
    @Getter private FactionsChat factionsChat;
    @Getter private ChatManager chatManager;

    @Override
    protected void onModuleEnable() throws Exception {
        instance = this;

        storageYML = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "storage.yml"));
        chatFilter = new ChatFilter();
        itemChat = new ItemChat();
        factionsChat = new FactionsChat();
        chatManager = new ChatManager();

        registerCommand(new FChatSpyCommand());
        registerCommand(new FilterCommand());
        registerCommand(new ReloadCommand());

        new FactionsChatCommand();
    }

    @Override
    protected void onModuleDisable() throws Exception {
        chatFilter.save();
        storageYML.save(new File(getDataFolder(), "storage.yml"));
    }
}
