package dev.creepah.chat.item;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public final class ItemChat {

    @Getter private Map<UUID, Long> cooldowns = new HashMap<>();

    public void sendItemChatMessage(Player player, ItemStack hand, String message) {
        net.minecraft.server.v1_8_R3.ItemStack nmsItem = CraftItemStack.asNMSCopy(hand);
        NBTTagCompound tag = new NBTTagCompound();
        nmsItem.save(tag);

        String[] msg = message.split("\\[item\\]");

        ChatComponentText itemText = new ChatComponentText(nmsItem.getName());
        ChatModifier modifier = itemText.getChatModifier();
        modifier.setColor(nmsItem.u().e);
        modifier.setChatHoverable(new ChatHoverable(ChatHoverable.EnumHoverAction.SHOW_ITEM, new ChatComponentText(tag.toString())));

        ChatComponentText master = new ChatComponentText("");
        master.addSibling(new ChatMessage(msg[0]));
        master.addSibling(itemText);
        if (msg.length > 1) master.addSibling(new ChatMessage(msg[1]));

        PlayerList list = MinecraftServer.getServer().getPlayerList();
        list.getPlayer(player.getName()).sendMessage(master);
    }
}
