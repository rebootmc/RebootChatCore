package dev.creepah.chat;

import com.massivecraft.factions.Rel;
import com.massivecraft.factions.entity.MPlayer;
import dev.creepah.chat.factionschat.ChatType;
import dev.creepah.chat.factionschat.FactionsChat;
import dev.creepah.chat.filter.ChatFilter;
import dev.creepah.chat.item.ItemChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;

public final class ChatManager {

    private final ChatCore i = ChatCore.get();

    public ChatManager() {
        i.observeEvent(EventPriority.HIGHEST, AsyncPlayerChatEvent.class)
                .subscribe(event -> {
                    if (event.isCancelled()) return; // Let other events have control - let cancelled events be cancelled!

                    ItemChat itemChat = i.getItemChat();
                    FactionsChat factionsChat = i.getFactionsChat();

                    Player player = event.getPlayer();
                    boolean fc = false;

                    // If they have a factions chat..
                    if (factionsChat.getChatType(player) != ChatType.PUBLIC) {
                        fc = true;

                        // Check if each recipient should receive the message, and remove them if not.
                        Iterator<Player> facIterator = event.getRecipients().iterator();
                        while (facIterator.hasNext()) {
                            Player recipient = facIterator.next();
                            Rel relationTo = MPlayer.get(recipient).getFaction().getRelationTo(MPlayer.get(event.getPlayer()).getFaction());

                            if (!factionsChat.getChatType(event.getPlayer()).getRelationships().contains(relationTo))
                                facIterator.remove();
                        }
                    }

                    // If we're dealing with an item message..
                    if (isItemChatMessage(event.getMessage())) {
                        // Ensure they have permission to use item chat.
                        if (!player.hasPermission("itemchat.use")) {
                            event.setCancelled(true);
                            player.sendMessage(i.formatAt("no-perm").get());
                            return;
                        }

                        // Check they have an item in their hand to show off.
                        ItemStack hand = player.getItemInHand();
                        if (hand == null || hand.getType() == Material.AIR) {
                            event.setCancelled(true);
                            player.sendMessage(i.formatAt("no-item").get());
                            return;
                        }

                        Map<UUID, Long> cooldowns = itemChat.getCooldowns();
                        int cooldown = i.getConfig().getInt("cooldown", 10);
                        long lastUsed = cooldowns.containsKey(player.getUniqueId()) ? cooldowns.get(player.getUniqueId()) : 0;

                        // Check that they haven't used it recently.
                        if (System.currentTimeMillis() - lastUsed >= (cooldown * 1000)) {
                            cooldowns.put(player.getUniqueId(), System.currentTimeMillis());
                        } else {
                            // Notify them of how long they have until they can use it again.
                            int timeLeft = (int) (((cooldown * 1000) - (System.currentTimeMillis() - lastUsed)) / 1000);
                            player.sendMessage(i.formatAt("cooldown").withModifier("time-left", timeLeft).get());
                            event.setCancelled(true);
                            return;
                        }
                    }

                    // If this is faction chat..
                    if (fc) {
                        // Send the message to those currently spying on faction chat.
                        Bukkit.getOnlinePlayers().stream().filter(factionsChat::isSpying).forEach(p -> {
                            String msg = getSpyMsg(player, getPlainMsg(p, event.getMessage()));

                            if (isItemChatMessage(msg))
                                itemChat.sendItemChatMessage(p, player.getItemInHand(), msg);
                            else
                                p.sendMessage(msg);
                        });
                    }

                    // For every player who should receive this message..
                    Iterator<Player> iterator = event.getRecipients().iterator();
                    while (iterator.hasNext()) {
                        Player r = iterator.next();
                        String msg = getPlainMsg(r, event.getMessage());
                        String message = fc ? getFactionMsg(player, r, msg, factionsChat.getChatType(player).getFormat()) : getFormattedMsg(player, r, msg);
                        message = message.replace("\\", "");

                        if (isItemChatMessage(message))
                            itemChat.sendItemChatMessage(r, player.getItemInHand(), message);
                        else r.sendMessage(message);

                        iterator.remove();
                    }
                });
    }


    // Format a normal chat message for the provided player, receiver and message.
    public String getFormattedMsg(Player player, Player receiver, String message) {
        return getChatFormat(player)
                .replace("{{faction}}", getFactionString(player, receiver))
                .replace("{{name}}", player.getDisplayName())
                .replace("{{message}}", message);
    }

    // Format a factions chat message in the provided format for the provided player, receiver and message.
    public String getFactionMsg(Player player, Player receiver, String message, String format) {
        return i.formatAt(format).get()
                .replace("{{faction}}", getFactionString(player, receiver))
                .replace("{{title}}", getTitle(MPlayer.get(player)))
                .replace("{{name}}", player.getDisplayName())
                .replace("{{message}}", Matcher.quoteReplacement(message));
    }


    // Format a faction chat message to be sent to players spying on faction chat.
    public String getSpyMsg(Player player, String message) {
        MPlayer m = MPlayer.get(player);
        return i.formatAt("fchat-spy-format")
                .withModifier("faction", m.getFaction().getName())
                .withModifier("title", getTitle(m))
                .withModifier("name", player.getDisplayName())
                .withModifier("message", message).get();
    }

    // Get the filtered or unfiltered message, but still unformatted message appropriate for the provided player.
    public String getPlainMsg(Player player, String message) {
        ChatFilter filter = ChatCore.get().getChatFilter();
        return Matcher.quoteReplacement(filter.isFilteringChat(player) ? filter.filterMessage(message) : message);
    }

    public String getTitle(MPlayer player) {
        return player.hasTitle() ? i.formatAt("title-format").get().replace("{{title}}", player.getTitle()) : "";
    }

    // Returns the chat format, ready to be formatted.
    public String getChatFormat(Player player) {
        ConfigurationSection section = i.getConfig().getConfigurationSection("formats");
        String format = section.getString("member");

        for (String key : section.getKeys(false)) if (player.hasPermission("rank." + key.toLowerCase())) format = section.getString(key);

        return ChatColor.translateAlternateColorCodes('&', format);
    }

    // Returns a faction string relevant to the two provided players.
    public String getFactionString(Player player, Player receiver) {
        MPlayer p = MPlayer.get(player), r = MPlayer.get(receiver);
        String colour = ChatColor.translateAlternateColorCodes('&', "&" + p.getFaction().getRelationTo(r.getFaction()).getColor().getChar());

        return p.getFaction().isNone() ? "" : colour + p.getRole().getPrefix() + p.getFaction().getName();
    }

    // Returns true if the provided message should be transformed into an item chat message.
    public boolean isItemChatMessage(String message) {
        return message.toLowerCase().contains("[item]");
    }
}
